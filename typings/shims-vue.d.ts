/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-22 16:33:16
 * @LastEditTime: 2022-11-24 11:21:22
 * @Description:
 * @FilePath: /web/typings/shims-vue.d.ts
 *
 */
/*the ts file of vue*/
declare module '*.vue' {
  import { DefineComponent } from 'vue'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>
  export default component
}

// declare module 'form-designer'
// declare module 'variant-form3'
declare module 'vform3-builds'
