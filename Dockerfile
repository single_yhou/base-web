FROM httpd:latest

COPY ./dist /usr/local/apache2/htdocs/
RUN echo 'LoadModule rewrite_module modules/mod_rewrite.so' >> /usr/local/apache2/conf/httpd.conf
RUN echo '<Directory />' >> /usr/local/apache2/conf/httpd.conf
RUN echo '    AllowOverride all' >> /usr/local/apache2/conf/httpd.conf
RUN echo '    Require all denied' >> /usr/local/apache2/conf/httpd.conf
RUN echo '</Directory>' >> /usr/local/apache2/conf/httpd.conf
EXPOSE 80
