const selfRoute = [
  {
    path: '/',
    component: ()=>import('@/layout/Layout.vue'),
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        component: () => import('@/views/dashboard/index.vue'),
        meta: { title: '数据概览', elSvgIcon: 'Fold' }
      }
    ]
  }
]

export default selfRoute