const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system',
        children: [
          {
            path: '/system/config',
            component: () => import('@/views/system/config/list.vue'),
            hidden: false,
            meta: { title: '系统数据配置' }
          },
          {
            path: '/system/config/detail',
            component: () => import('@/views/system/config/detail.vue'),
            hidden: true,
            meta: { title: '系统数据配置' }
          },
          {
            path: '/system/config/detail/:id',
            component: () => import('@/views/system/config/detail.vue'),
            hidden: true,
            meta: { title: '系统数据配置' }
          }
        ]
      }
    ]
  }
]

export default route
