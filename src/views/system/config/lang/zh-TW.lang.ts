let systemConfig = {
  systemConfig: {
    table: {
      systemCode: '系統編碼', // 系统编码
      dataValue: '數據值', // 数据值
      state: '狀態', // 状态
      isEnabled: '是否啟用', //是否启用
      enabled: '啟用', // 启用
      disabled: '禁用' // 禁用
    }
  }
}
export default systemConfig
