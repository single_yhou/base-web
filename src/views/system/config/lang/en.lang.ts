let systemConfig = {
    systemConfig: {
        table: {
          systemCode: 'SystemCode', // 系统编码
          dataValue: 'DataValue', // 数据值
          state: 'State', // 状态
          isEnabled: 'IsEnabled', //是否启用
          enabled: 'Enabled', // 启用
          disabled: 'Disabled' // 禁用
        }
      },
    
  }
  export default systemConfig
  