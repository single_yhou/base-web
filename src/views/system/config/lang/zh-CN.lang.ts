/*
 * @Author: Chuckie
 * @Email: chuckie@slanwell.com
 * @Date: 2023-06-05 11:06:52
 * @LastEditTime: 2023-06-05 16:45:40
 * @Description:
 * @FilePath: /base-web/src/views/system/config/lang/zh-CN.lang.ts
 *
 */
const systemConfig = {
  systemConfig: {
    table: {
      name: '名称', // 系统编码
      code: '编码', // 数据值
      type: '类型', // 状态
      value: '值' //是否启用
    }
  }
}
export default systemConfig
