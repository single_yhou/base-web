/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2023-06-05 16:52:41
 * @Description:
 * @FilePath: /base-web/src/views/system/config/api/config.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface Config {
  gmtCreate: string
  createBy: string
  id: number
  name: string
  code: string
  type: string
  value: string
}

const baseUrl = '/base/system/config'

export const select = (params: ObjTy) => {
  return axiosReq({
    url: baseUrl,
    method: 'get',
    isParams: true,
    data: params
  })
}

export const selectOne = (id: string) => {
  return axiosReq({
    url: baseUrl + `/${id}`,
    method: 'get'
  })
}

export const create = (entity: Config) => {
  return axiosReq({
    url: baseUrl,
    method: 'post',
    data: entity
  })
}

export const update = (entity: Config) => {
  return axiosReq({
    url: baseUrl,
    method: 'put',
    data: entity
  })
}

export const remove = (ids: Array<string>) => {
  return axiosReq({
    url: baseUrl,
    method: 'delete',
    data: ids
  })
}
