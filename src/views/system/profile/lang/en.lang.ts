let profile = {
  profile: {
    innerLetter: 'Innerletter', // 站内信
    read: 'Read', // 已读
    personalCenter: 'PersonalCenter', // 个人中心
    changePassword: 'ChangePassword', // 修改密码
    logout: 'Logout', // 退出登录
    personalResource: 'PersonalResource', // 个人资料
    account: 'Account', // 账号
    nickName: 'NickName', // 昵称
    email: 'Email', // 邮箱
    phone: 'Phone', // 手机
    language: 'Language', // 语言
    english: 'English', // 英语
    chineseSimplified: 'ChineseSimplified', // 中文简体
    chineseTaiwan: 'ChineseTaiwan', // 中文繁体
    timeZone: 'timeZone' // 时区
  }
}
export default profile
