let profile = {
  profile: {
    innerLetter: '站内信', // 站内信
    read: '已读', // 已读
    personalCenter: '个人中心', // 个人中心
    changePassword: '修改密码', // 修改密码
    logout: '退出登录', // 退出登录
    personalResource: '个人资料', // 个人资料
    account: '账号', // 账号
    nickName: '昵称', // 昵称
    email: '邮箱', // 邮箱
    phone: '手机', // 手机
    language: '语言', // 语言
    english: '英语', // 英语
    chineseSimplified: '中文简体', // 中文简体
    chineseTaiwan: '中文繁体', // 中文繁体
    timeZone: '时区' // 時區
  }
}
export default profile
