let profile = {
  profile: {
    innerLetter: '站內信', // 站内信
    read: '已讀', // 已读
    personalCenter: '個人中心', // 个人中心
    changePassword: '修改密碼', // 修改密码
    logout: '退出登錄', // 退出登录
    personalResource: '個人資料', // 个人资料
    account: '帳號', // 账号
    nickName: '昵稱', // 昵称
    email: '郵箱', // 邮箱
    phone: '手機', // 手机
    language: '語言', // 语言
    english: '英語', // 英语
    chineseSimplified: '中文簡體', // 中文简体
    chineseTaiwan: '中文繁體', // 中文繁体
    timeZone: '時區' // 時區
  },

  }
  export default profile
  