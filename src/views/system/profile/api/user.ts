/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-10 13:13:11
 * @LastEditTime: 2023-03-30 15:02:55
 * @Description:
 * @FilePath: /web/src/api/user.ts
 *
 */
import request from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export const loginReq = async (data: ObjTy) => {
  return request({
    url: '/login',
    data,
    method: 'post',
    bfLoading: false,
    isParams: false,
    isAlertErrorMsg: false
  })
}

export function getInfoReq() {
  return request({
    url: '/profile',
    bfLoading: false,
    method: 'get',
    isAlertErrorMsg: false
  })
}

export function getMenuLanguage() {
  return request({
    url: '/language/dict/all',
    bfLoading: false,
    method: 'get',
    isAlertErrorMsg: false
  })
}

export function logoutReq() {
  return request({
    url: '/logout',
    method: 'get'
  })
}

/**
 * 获取钉钉的系统信息
 * @returns 钉钉信息
 */
export function getDingDingSystem() {
  return request({
    url: '/base/ding/url',
    method: 'get'
  })
}

/**
 * 用户获取临时钉钉凭证
 * @param userid 用户id
 */
export function getStateToken(userId) {
  return request({
    url: 'base/ding/token',
    method: 'get',
    isParams: true,
    data: {
      userId
    }
  })
}

/**
 *
 * @param authCode 钉钉授权码
 * @param state  钉钉临时凭证
 * @returns
 */
export function dingdingAuth(authCode: string, state: string) {
  return request({
    url: '/base/ding/auth',
    method: 'get',
    isParams: true,
    data: {
      authCode,
      state
    }
  })
}
