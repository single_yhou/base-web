const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/basic/log',
        component: () => import('@/views/system/log/index.vue'),
        name: 'Log',
        meta: { title: '日志' }
      }
    ]
  }
]

export default route
