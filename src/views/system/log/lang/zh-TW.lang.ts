/* 日志管理 */
let basicLog = {
  /* 日志管理 */
  basicLog: {
    search: {
      logType: '日誌類型' // 日志类型
    },
    table: {
      logType: '日誌類型', // 日志类型
      URL: '請求地址', // 请求地址
      description: '操作描述', // 操作描述
      content: '操作內容', // 操作内容
      operator: '操作人', // 操作人
      operateTime: '操作時間' // 操作时间
    },
    operation: {}
  }
}

export default basicLog
