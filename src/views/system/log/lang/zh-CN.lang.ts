/* 日志管理 */
let basicLog = {
  basicLog: {
    search: {
      logType: '日志类型' // 日志类型
    },
    table: {
      logType: '日志类型', // 日志类型
      URL: '请求地址', // 请求地址
      description: '操作描述', // 操作描述
      content: '操作内容', // 操作内容
      operator: '操作人', // 操作人
      operateTime: '操作时间' // 操作时间
    },
    operation: {}
  }
}

export default basicLog
