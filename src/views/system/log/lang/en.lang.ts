/* 日志管理 */
let basicLog = {
  /* 日志管理 */
  basicLog: {
    search: {
      logType: 'LogType' // 日志类型
    },
    table: {
      logType: 'LogType', // 日志类型
      URL: 'URL', // 请求地址
      description: 'Description', // 操作描述
      content: 'Content', // 操作内容
      operator: 'Operator', // 操作人
      operateTime: 'OperateTime' // 操作时间
    },
    operation: {}
  }
}

export default basicLog
