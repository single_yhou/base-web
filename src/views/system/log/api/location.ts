/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2023-04-07 11:06:03
 * @Description:
 * @FilePath: /web/src/api/basic/location.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface Location {
  gmtCreate?: string
  createBy?: string
  id?: number
  code?: string
  agvCode?: string
  name?: string
  parentId?: number
  type?: string
  typeName?: string
  storeType?: string
  des?: string
  enable?: boolean
  modifyBy?: string
  gmtModify?: string
  x?: string
  y?: string
  active?: boolean
}

export const select = (params: ObjTy) => {
  return axiosReq({
    url: '/base/business/position',
    method: 'get',
    isParams: true,
    data: params
  })
}

export const selectOne = (id: string) => {
  return axiosReq({
    url: `/base/business/position/${id}`,
    method: 'get'
  })
}

export const create = (entity: Location) => {
  return axiosReq({
    url: '/base/business/position',
    method: 'post',
    data: entity
  })
}

export const update = (entity: Location) => {
  return axiosReq({
    url: '/base/business/position',
    method: 'put',
    data: entity
  })
}

export const remove = (ids: Array<string>) => {
  return axiosReq({
    url: '/base/business/position',
    method: 'delete',
    data: ids
  })
}
