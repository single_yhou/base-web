/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2022-11-12 14:26:31
 * @Description:
 * @FilePath: /zl-web/src/api/enterprise.ts
 *
 */
export class BaseLog {
    content?: string
    createBy?: string
    createName?: string
    datetime?: string
    gmtCreate?: string
    id?: string
    operation?: string
    requestUrl?: string
    type?: string
    username?: string
}
import { BaseAPI } from './base-api'

export const BaseLogAPI = new BaseAPI("/base/log")

