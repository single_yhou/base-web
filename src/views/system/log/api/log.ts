/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2023-04-03 16:50:04
 * @Description:
 * @FilePath: /web/src/api/basic/log.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface Log {
  id: number
  gmtCreate: string
  createBy: string
  type: string
  requestUrl: string
  operation: string
  content: string
  username: string
  datetime: string
}

const baseUrl = '/base/log'

export const select = (params: ObjTy) => {
  return axiosReq({
    url: baseUrl,
    method: 'get',
    isParams: true,
    data: params
  })
}
