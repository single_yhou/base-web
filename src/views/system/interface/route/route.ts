const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system/interface',
        component: () => import('@/views/system/interface/list.vue'),
        meta: { title: '接口管理' }
      },
      {
        path: '/system/interface/detail',
        component: () => import('@/views/system/interface/detail.vue'),
        meta: { title: '添加接口' }
      },
      {
        path: '/system/interface/detail/:id',
        component: () => import('@/views/system/interface/detail.vue'),
        meta: { title: '修改接口' }
      }
    ]
  }
]

export default route
