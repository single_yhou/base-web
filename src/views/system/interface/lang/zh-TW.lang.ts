let systemInterface = {
  systemInterface: {
    search: {
      name: '介面名稱' // 接口名称
    },

    table: {
      interfaceName: '介面名稱', // 接口名称
      interfaceURL: '介面URL', // 接口URL
      configData: '配置數據', //  配置数据
      interfaceCode: '介面編碼' //接口编码
    }
  }
}
export default systemInterface
