let systemInterface = {
  systemInterface: {
    search: {
      name: 'InterfaceName' // 接口名称
    },

    table: {
      interfaceName: 'InterfaceName', // 接口名称
      interfaceURL: 'InterfaceURL', // 接口URL
      configData: 'ConfigData', //  配置数据
      interfaceCode: 'InterfaceCode' //接口编码
    }
  },
  }
  export default systemInterface
  