let systemInterface = {
  systemInterface: {
    search: {
      name: '接口名称' // 接口名称
    },

    table: {
      interfaceName: '接口名称', // 接口名称
      interfaceURL: '接口URL', // 接口URL
      configData: '配置数据', //  配置数据
      interfaceCode: '接口编码', //接口编码
      httpMethod: '请求类型', //请求类型
      des: '描述' //描述
    }
  }
}
export default systemInterface
