const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system/dept',
        component: () => import('@/views/system/dept/list.vue'),
        name: 'Dept',
        hidden: false,
        meta: { title: '组织结构' }
      },
      {
        path: '/system/dept/detail',
        component: () => import('@/views/system/dept/detail.vue'),
        name: 'DeptCreate',
        hidden: true,
        meta: { title: '新增组织结构' }
      },
      {
        path: '/system/dept/detail/:id',
        component: () => import('@/views/system/dept/detail.vue'),
        name: 'DeptEdit',
        hidden: true,
        meta: { title: '编辑组织结构' }
      },
      {
        path: '/system/dept/add/:id',
        component: () => import('@/views/system/dept/add.vue'),
        name: 'DeptAdd',
        hidden: true,
        meta: { title: '添加组织结构' }
      }
    ]
  }
]

export default route
