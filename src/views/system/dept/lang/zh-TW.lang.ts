/* 组织结构 */
let systemDept = {
  /* 组织结构 */
  systemDept: {
    search: {
      type: '類型' // 类型
    },
    table: {
      shortName: '簡稱', // 简称
      fullName: '全稱', // 全称
      abbreviation: '縮寫', // 缩写
      orderNum: '排序號', // 排序号
      type: '機構類型', // 机构类型
      remark: '備註' // 备注
    },
    operation: {
      addChildren: '添加子組織' // 添加子组织
    }
  }
}
export default systemDept
