/* 组织结构 */
let systemDept = {
     /* 组织结构 */
  systemDept: {
    search: {
      type: 'Type' // 类型
    },
    table: {
      shortName: 'ShortName', // 简称
      fullName: 'FullName', // 全称
      abbreviation: 'Abbreviation', // 缩写
      orderNum: 'OrderNumber', // 排序号
      type: 'Type', // 机构类型
      remark: 'Remark' // 备注
    },
    operation: {
      addChildren: 'Add children' // 添加子组织
    }
  },

  }
  export default systemDept
  