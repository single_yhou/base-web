/* 组织结构 */
let systemDept = {
  systemDept: {
    search: {
      type: '类型' // 类型
    },
    table: {
      shortName: '简称', // 简称
      fullName: '全称', // 全称
      abbreviation: '缩写', // 缩写
      orderNum: '排序号', // 排序号
      type: '机构类型', // 机构类型
      remark: '备注' // 备注
    },
    operation: {
      addChildren: '添加子组织' // 添加子组织
    }
  }
}
export default systemDept
