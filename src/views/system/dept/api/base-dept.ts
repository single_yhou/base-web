/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2022-11-12 14:26:31
 * @Description:
 * @FilePath: /zl-web/src/api/enterprise.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface BaseDept {
  id?:string
num?:string
pid?:string
simpleName?:string
fullName?:string
insCode?:string
version?:string
remark?:string
status?:string
gmtCreate?:string
createBy?:string
gmtModify?:string
modifyBy?:string
type?:string
relCompanyNo?:string
relCompanyName?:string

}

export const select = (params: ObjTy) => {
  return axiosReq({
    url: '/base/dept',
    method: 'get',
    isParams: true,
    data: params
  })
}

export const selectOne = (id: string) => {
  return axiosReq({
    url: `/base/dept/${id}`,
    method: 'get'
  })
}

export const create = (entity: BaseDept) => {
  return axiosReq({
    url: '/base/dept',
    method: 'post',
    data: entity
  })
}

export const update = (entity: BaseDept) => {
  return axiosReq({
    url: '/base/dept',
    method: 'put',
    data: entity
  })
}

export const remove = (ids: Array<string>) => {
  return axiosReq({
    url: '/base/dept',
    method: 'delete',
    data: ids
  })
}
export const selectPolicyTree = () => {
  return axiosReq({
    url: '/base/dept/tree',
    method: 'get'
  })
}

 