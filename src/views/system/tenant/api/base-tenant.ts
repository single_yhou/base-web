/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2022-11-15 09:57:30
 * @Description:
 * @FilePath: /zl-web/src/api/base-role.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface BaseTenant {
  code: string
  id: string
  name: string
  pid: string
  des: string
}

export const select = (params: ObjTy) => {
  return axiosReq({
    url: '/base/tenant',
    method: 'get',
    isParams: true,
    data: params
  })
}

export const selectIds = (ids: number[]) => {
  return axiosReq({
    url: `/base/tenant/list/${ids}`,
    method: 'get'
  })
}

export const selectOne = (id: string) => {
  return axiosReq({
    url: `/base/tenant/${id}`,
    method: 'get'
  })
}

export const create = (entity: BaseTenant) => {
  return axiosReq({
    url: '/base/tenant',
    method: 'post',
    data: entity
  })
}

export const update = (entity: BaseTenant) => {
  return axiosReq({
    url: '/base/tenant',
    method: 'put',
    data: entity
  })
}

export const remove = (ids: Array<string>) => {
  return axiosReq({
    url: '/base/tenant',
    method: 'delete',
    data: ids
  })
}
