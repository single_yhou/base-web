const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system/tenant',
        component: () => import('@/views/system/tenant/list.vue'),
        hidden: true,
        meta: { title: '租户列表' }
      },
      {
        path: '/system/tenant/detail',
        component: () => import('@/views/system/tenant/detail.vue'),
        hidden: true,
        meta: { title: '租户详情' }
      },
      {
        path: '/system/tenant/detail/:id',
        component: () => import('@/views/system/tenant/detail.vue'),
        hidden: true,
        meta: { title: '租户编辑' }
      }
    ]
  }
]

export default route
