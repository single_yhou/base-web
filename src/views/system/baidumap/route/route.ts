const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system',
        children: [
          {
            path: '/system/baidumap',
            component: () => import('@/views/system/baidumap/map.vue'),
            meta: { title: '地图' }
          },
           
        ]
      }
    ]
  }
]

export default route
