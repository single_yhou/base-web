/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2023-04-04 11:04:49
 * @Description:
 * @FilePath: /web/src/api/system/dict-info.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface DictInfo {
  code: string
  gmtCreate: string
  id: number
  name: string
  pid: number
  typeCode: string
  typeId: number
  typeName: string
}

export const select = (params: ObjTy) => {
  return axiosReq({
    url: '/base/dict/info',
    method: 'get',
    isParams: true,
    data: params
  })
}

export const selectIds = (ids: number[]) => {
  return axiosReq({
    url: `/base/dict/info/list/${ids}`,
    method: 'get'
  })
}

export const selectOne = (id: string) => {
  return axiosReq({
    url: `/base/dict/info/${id}`,
    method: 'get'
  })
}

export const selectByTypeCode = (code: string) => {
  return axiosReq({
    url: `/base/dict/info/type/${code}`,
    method: 'get'
  })
}

export const create = (entity: DictInfo) => {
  return axiosReq({
    url: '/base/dict/info',
    method: 'post',
    data: entity
  })
}

export const update = (entity: DictInfo) => {
  return axiosReq({
    url: '/base/dict/info',
    method: 'put',
    data: entity
  })
}

export const remove = (ids: Array<string>) => {
  return axiosReq({
    url: '/base/dict/info',
    method: 'delete',
    data: ids
  })
}
