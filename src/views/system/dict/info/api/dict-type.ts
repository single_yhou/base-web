/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2023-04-04 10:39:57
 * @Description:
 * @FilePath: /web/src/api/system/dict-type.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface DictType {
  cascadeFlag: number
  createBy: string
  gmtCreate: string
  id: number
  type: string
  typeName: string
}

export const select = (params: ObjTy) => {
  return axiosReq({
    url: '/base/dict',
    method: 'get',
    isParams: true,
    data: params
  })
}

export const selectIds = (ids: number[]) => {
  return axiosReq({
    url: `/base/dict/list/${ids}`,
    method: 'get'
  })
}

export const selectOne = (id: string) => {
  return axiosReq({
    url: `/base/dict/${id}`,
    method: 'get'
  })
}

export const create = (entity: DictType) => {
  return axiosReq({
    url: '/base/dict',
    method: 'post',
    data: entity
  })
}

export const update = (entity: DictType) => {
  return axiosReq({
    url: '/base/dict',
    method: 'put',
    data: entity
  })
}

export const remove = (ids: Array<string>) => {
  return axiosReq({
    url: '/base/dict',
    method: 'delete',
    data: ids
  })
}
