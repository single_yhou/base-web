/* 字典配置 */
let sysDictInfo = {
  sysDictInfo: {
    search: {
      type: '类型' // 类型
    },
    table: {
      type: '类型', // 类型
      typeCode: '类型代码', // 类型代码
      name: '名称', // 名称
      code: '代码', // 代码
      parentId: '父级id' // 父级id
    }
  }
}
export default sysDictInfo
