const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system/dict/info',
        component: () => import('@/views/system/dict/info/infoList.vue'),
        hidden: false,
        meta: { title: '字典配置' }
      },
      {
        path: '/system/dict/info/detail',
        component: () => import('@/views/system/dict/info/infoDetail.vue'),
        hidden: true,
        meta: { title: '字典配置' }
      },
      {
        path: '/system/dict/info/detail/:id',
        component: () => import('@/views/system/dict/info/infoDetail.vue'),
        hidden: true,
        meta: { title: '字典配置' }
      }
    ]
  }
]

export default route
