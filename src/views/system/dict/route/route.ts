const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system/dict',
        component: () => import('@/views/system/dict/list.vue'),
        hidden: false,
        meta: { title: '字典类型管理' }
      },
      {
        path: '/system/dict/detail',
        component: () => import('@/views/system/dict/detail.vue'),
        hidden: true,
        meta: { title: '字典类型管理' }
      }
    ]
  }
]

export default route
