/* 字典类型管理 */
let sysDict = {
  sysDict: {
    table: {
      typeName: '类型名称', // 类型名称
      typeCode: '类型代码', // 类型代码
      isCascade: '是否级联' // 是否级联
    }
  }
}

export default sysDict
