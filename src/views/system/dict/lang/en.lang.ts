/* 字典类型管理 */
let sysDict = {
  /* 字典类型管理 */
  sysDict: {
    table: {
      typeName: 'TypeName', // 类型名称
      typeCode: 'TypeCode', // 类型代码
      isCascade: 'IsCascade' // 是否级联
    }
  }
}

export default sysDict
