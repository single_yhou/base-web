/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2023-03-30 14:55:14
 * @Description:
 * @FilePath: /web/src/api/system/config.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface Language {
  id?: string
  code?: string
  english?: string
  chineseSimplified?: string
  chineseTaiwan?: string
}
const baseUrl = '/language/dict'

export const select = (params: ObjTy) => {
  return axiosReq({
    url: baseUrl,
    method: 'get',
    isParams: true,
    data: params
  })
}

export const selectOne = (id: string) => {
  return axiosReq({
    url: baseUrl + `/${id}`,
    method: 'get'
  })
}

export const create = (entity: Language) => {
  return axiosReq({
    url: baseUrl,
    method: 'post',
    data: entity
  })
}

export const update = (entity: Language) => {
  return axiosReq({
    url: baseUrl,
    method: 'put',
    data: entity
  })
}

export const remove = (ids: Array<string>) => {
  return axiosReq({
    url: baseUrl,
    method: 'delete',
    data: ids
  })
}
