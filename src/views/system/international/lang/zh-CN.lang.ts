/* 国际化 */
let international = {
  international: {
    search: {},
    table: {
      english: '英语', // 英语
      code: '代码', // 代码
      chineseSimplified: '简体中文', // 简体中文
      chineseTaiwan: '繁体中文' // 繁体中文
    }
  }
}
export default international
