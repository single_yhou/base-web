/* 国际化 */
let international = {
     /* 国际化 */
  international: {
    search: {},
    table: {
      english: 'english', // 英语
      code: 'code', // 代码
      chineseSimplified: 'chineseSimplified', // 简体中文
      chineseTaiwan: 'chineseTaiwan' // 繁体中文
    }
  }
  }
  export default international
  