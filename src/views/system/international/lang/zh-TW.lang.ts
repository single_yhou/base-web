/* 国际化 */
let international = {
  /* 国际化 */
  international: {
    search: {},
    table: {
      english: '英語', // 英语
      code: '代碼', // 代码
      chineseSimplified: '簡體中文', // 简体中文
      chineseTaiwan: '繁體中文' // 繁体中文
    }
  }
}
export default international
