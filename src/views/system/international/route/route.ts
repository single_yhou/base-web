const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system/international',
        component: () => import('@/views/system/international/list.vue'),
        hidden: false,
        meta: { title: '国际化' }
      },
      {
        path: '/system/international/detail',
        component: () => import('@/views/system/international/detail.vue'),
        hidden: false,
        meta: { title: '新增,修改' }
      },
      {
        path: '/system/international/detail/:id',
        component: () => import('@/views/system/international/detail.vue'),
        hidden: false,
        meta: { title: '新增,修改' }
      }
    ]
  }
]

export default route
