const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system/role',
        component: () => import('@/views/system/role/list.vue'),
        name: 'Role',
        meta: { title: '角色' }
      },
      {
        path: '/system/role/detail',
        component: () => import('@/views/system/role/detail.vue'),
        name: 'RoleDetailCreate',
        hidden: true,
        meta: { title: '新增角色' }
      },
      {
        path: '/system/role/detail/:id',
        component: () => import('@/views/system/role/detail.vue'),
        name: 'RoleDetialEdit',
        hidden: true,
        meta: { title: '编辑角色' }
      },
      {
        path: '/system/role/add/policy/:id',
        component: () => import('@/views/system/role/add-policy.vue'),
        name: 'RoleAddPolicy',
        hidden: true,
        meta: { title: '分配权限' }
      }
    ]
  }
]

export default route
