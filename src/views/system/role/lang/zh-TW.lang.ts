let role = {
  role: {
    table: {
      roleName: '角色名', //角色名
      enabled: '是否可用', // 是否可用
      roleCode: '角色編碼', // 角色编码
      type: '類型', // 类型
      yes: '是', // 是
      no: '否' // 否
    },
    operation: {
      assignPolicy: '分配權限' // 分配权限
    }
  }
}
export default role
