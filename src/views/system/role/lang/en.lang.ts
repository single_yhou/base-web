let role = {
  role: {
    table: {
      roleName: 'RoleName', //角色名
      enabled: 'Enabled', // 是否可用
      roleCode: 'RoleCode', // 角色编码
      type: 'Type', // 类型
      yes: 'Yes', // 是
      no: 'No' // 否
    },
    operation: {
      assignPolicy: 'AssignPolicy' // 分配权限
    }
  }
}
export default role
