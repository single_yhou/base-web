/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2023-03-30 15:02:04
 * @Description:
 * @FilePath: /web/src/api/base-user.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface BaseUser {
  id?: string
  username?: string
  password?: string
  salt?: string
  email?: string
  phone?: string
  sex?: number
  age?: number
  status?: number
  createBy?: string
  modifyBy?: string
  gmtCreate?: string
  gmtModify?: string
  lastLoginTime?: string
  nickname?: string
  // dingflag?:boolean,
}

export const select = (params: ObjTy) => {
  return axiosReq({
    url: '/base/user',
    method: 'get',
    isParams: true,
    data: params
  })
}

export const selectOne = (id: string) => {
  return axiosReq({
    url: `/base/user/${id}`,
    method: 'get'
  })
}

export const create = (entity: BaseUser) => {
  return axiosReq({
    url: '/base/user',
    method: 'post',
    data: entity
  })
}

export const update = (entity: BaseUser) => {
  return axiosReq({
    url: '/base/user',
    method: 'put',
    data: entity
  })
}

export const remove = (ids: Array<string>) => {
  return axiosReq({
    url: '/base/user',
    method: 'delete',
    data: ids
  })
}

interface addRole {
  roleIds: Array<string>
  userId: string | string[]
}
export const add = (entity: addRole) => {
  return axiosReq({
    url: '/base/user/role/add',
    method: 'post',
    data: entity
  })
}

export const selectRoleIdByUserId = (id: string | string[]) => {
  return axiosReq({
    url: `/base/role/user/${id}`,
    method: 'get'
  })
}
