const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system/user',
        component: () => import('@/views/system/user/list.vue'),
        hidden: true,
        meta: { title: '用户列表' }
      },
      {
        path: '/system/user/detail/:id',
        component: () => import('@/views/system/user/detail.vue'),
        hidden: true,
        meta: { title: '用户详情' }
      },
      {
        path: '/system/user/add/role/:id',
        component: () => import('@/views/system/user/add-role.vue'),
        hidden: true,
        meta: { title: '分配角色' }
      }
    ]
  }
]

export default route
