let systemUser = {
  systemUser: {
    table: {
      userName: '用戶名', // 用户名
      nickName: '昵稱', // 昵称
      email: '郵箱', // 邮箱
      phone: '聯繫方式', // 联系方式
      sex: '性別', // 性别
      age: '年齡', // 年龄
      userState: '用戶狀態', // 用户状态
      latestLogin: '最後登錄時間', // 最后登录时间
      man: '男', // 男
      woman: '女', // 女
      state: '狀態', // 状态
      normal: '正常', // 正常
      disabled: '禁用' // 禁用
    },
    operation: {
      assignRole: '分配角色' // 分配角色
    },
    detail: {
      password: '密碼', // 密码
      phone: '手機號' // 手机号
    }
  }
}
export default systemUser
