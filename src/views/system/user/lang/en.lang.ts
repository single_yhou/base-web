let systemUser = {
  systemUser: {
    table: {
      userName: 'UserName', // 用户名
      nickName: 'NickName', // 昵称
      email: 'Email', // 邮箱
      phone: 'Phone', // 联系方式
      sex: 'Sex', // 性别
      age: 'Age', // 年龄
      userState: 'UserState', // 用户状态
      latestLogin: 'LatestLogin', // 最后登录时间
      man: 'Man', // 男
      woman: 'Woman', // 女
      state: '状态', // 状态
      normal: 'Normal', // 正常
      disabled: 'Disabled' // 禁用
    },
    operation: {
      assignRole: 'AssignRole' // 分配角色
    },
    detail: {
      password: 'Password', // 密码
      phone: 'Phone' // 手机号
    }
  }
}
export default systemUser
