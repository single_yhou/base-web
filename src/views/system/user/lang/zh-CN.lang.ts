let systemUser = {
  systemUser: {
    table: {
      userName: '用户名', // 用户名
      nickName: '昵称', // 昵称
      email: '邮箱', // 邮箱
      phone: '联系方式', // 联系方式
      sex: '性别', // 性别
      age: '年龄', // 年龄
      userState: '用户状态', // 用户状态
      latestLogin: '最后登录时间', // 最后登录时间
      man: '男', // 男
      woman: '女', // 女
      state: '状态', // 状态
      normal: '正常', // 正常
      disabled: '禁用' // 禁用
    },
    operation: {
      assignRole: '分配角色' // 分配角色
    },
    detail: {
      password: '密码', // 密码
      phone: '手机号' // 手机号
    }
  }
}
export default systemUser
