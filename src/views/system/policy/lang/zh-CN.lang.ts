/*
 * @Author: Chuckie
 * @Email: chuckie@slanwell.com
 * @Date: 2023-06-05 11:06:52
 * @LastEditTime: 2023-06-05 18:21:52
 * @Description:
 * @FilePath: /base-web/src/views/system/policy/lang/zh-CN.lang.ts
 *
 */
/* 菜单管理 */
const policy = {
  policy: {
    search: {
      addRootPolicy: '新增顶级权限' // 新增顶级权限
    },
    table: {
      policyName: '权限名称', // 权限名称
      description: '权限描述', // 权限描述
      URL: '权限访问路径', // 权限访问路径
      flag: '权限标识', // 权限标识
      type: '类型', // 类型
      order: '排序', // 排序
      icon: '图标' // 图标
    },
    operation: {
      addChildren: '加子级' //添加子级
    },
    detail: {
      menu: '菜单', // 菜单
      page: '页面', // 页面
      button: '按钮', // 按钮
      chooseIcon: '选择图标' // 选择图标
    }
  }
}
export default policy
