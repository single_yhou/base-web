/* 菜单管理 */
let policy = {
  /* 菜单管理 */
  policy: {
    search: {
      addRootPolicy: '新增頂級許可權' // 新增顶级权限
    },
    table: {
      policyName: '許可權名稱', // 权限名称
      description: '許可權描述', // 权限描述
      URL: '許可權訪問路徑', // 权限访问路径
      flag: '許可權標識', // 权限标识
      type: '類型', // 类型
      order: '排序', // 排序
      icon: '圖示' // 图标
    },
    operation: {
      addChildren: '添加子級' //添加子级
    },
    detail: {
      menu: '菜單', //菜单
      page: '頁面', //页面
      button: '按鈕', // 按钮
      chooseIcon: '選擇圖示' //选择图标
    }
  }
}
export default policy
