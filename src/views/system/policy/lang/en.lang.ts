/* 菜单管理 */
let policy = {
  /* 菜单管理 */
  policy: {
    search: {
      addRootPolicy: 'AddRootPolicy' // 新增顶级权限
    },
    table: {
      policyName: 'PolicyName', // 权限名称
      description: 'Description', // 权限描述
      URL: 'URL', // 权限访问路径
      flag: 'Flag', // 权限标识
      type: 'Type', // 类型
      order: 'Order', // 排序
      icon: 'Icon' // 图标
    },
    operation: {
      addChildren: 'AddChildren' //添加子级
    },
    detail: {
      menu: 'Menu', //菜单
      page: 'Page', //页面
      button: 'Button', // 按钮
      chooseIcon: 'ChooseIcon' //选择图标
    }
  }
}
export default policy
