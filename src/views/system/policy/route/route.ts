const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/system/policy',
        component: () => import('@/views/system/policy/list.vue'),
        name: 'Policy',
        meta: { title: '权限' }
      },
      {
        path: '/system/policy/detail',
        component: () => import('@/views/system/policy/detail.vue'),
        name: 'PolicyCreate',
        hidden: true,
        meta: { title: '新增权限' }
      },
      {
        path: '/system/policy/detail/:id',
        component: () => import('@/views/system/policy/detail.vue'),
        name: 'PolicyEdit',
        hidden: true,
        meta: { title: '编辑权限' }
      },
      {
        path: '/system/policy/add/children/:id',
        component: () => import('@/views/system/policy/detail.vue'),
        name: 'PolicyAddChildren',
        hidden: true,
        meta: { title: '添加子级' }
      },
      {
        path: '/system/policy/add/:id',
        component: () => import('@/views/system/policy/add.vue'),
        name: 'Add',
        hidden: true,
        meta: { title: '添加权限' }
      }
    ]
  }
]

export default route
