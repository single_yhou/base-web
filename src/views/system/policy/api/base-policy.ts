/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2022-11-12 14:26:31
 * @Description:
 * @FilePath: /zl-web/src/api/enterprise.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface BasePolicy {
  id?: string
  name?: string
  description?: string
  url?: string
  perms?: string
  parentId?: string
  type?: string
  orderNum?: string
  icon?: string
  status?: string
  createBy?: string
  modifyBy?: string
  gmtCreate?: string
  gmtmodify?: string
  children?:Array<any>
}

export const select = (params: ObjTy) => {
  return axiosReq({
    url: '/base/permission',
    method: 'get',
    isParams: true,
    data: params
  })
}

export const selectOne = (id: string) => {
  return axiosReq({
    url: `/base/permission/${id}`,
    method: 'get'
  })
}

export const create = (entity: BasePolicy) => {
  return axiosReq({
    url: '/base/permission',
    method: 'post',
    data: entity
  })
}

export const update = (entity: BasePolicy) => {
  return axiosReq({
    url: '/base/permission',
    method: 'put',
    data: entity
  })
}

export const remove = (ids: Array<string>) => {
  return axiosReq({
    url: '/base/permission',
    method: 'delete',
    data: ids
  })
}
export const selectPolicyTree = () => {
  return axiosReq({
    url: '/base/permission/tree',
    method: 'get'
  })
}

export const selectPolicyTreeByRoleId = (roleId: string | string[]) => {
  return axiosReq({
    url: `/base/permission/tree/role/${roleId}`,
    method: 'get'
  })
}

interface addPolicyByRoleId {
  permissionIds: Array<any>
  roleId: any
}

export const addPolicyByRoleId = (addPolicyByRoleId: addPolicyByRoleId) => {
  return axiosReq({
    url: `/base/role/permission/add`,
    method: 'post',
    data: addPolicyByRoleId
  })
}
