/*
 * @Author: Chuckie
 * @Email: chuckie@slanwell.com
 * @Date: 2023-06-05 13:56:24
 * @LastEditTime: 2023-06-06 15:34:34
 * @Description:
 * @FilePath: /base-web/src/views/lowcode/route/route.ts
 *
 */
const route = [
  {
    path: '/',
    component: () => import('@/layout/Layout.vue'),
    redirect: 'noRedirect',
    children: [
      {
        path: '/lowcode/preview/:code',
        component: () => import('@/views/lowcode/preview.vue'),
        meta: { title: '预览' }
      },
      {
        path: '/lowcode/designer',
        component: () => import('@/views/lowcode/designer.vue'),
        meta: { title: '设计表' }
      },
      {
        path: '/lowcode/designer/:id',
        component: () => import('@/views/lowcode/designer.vue'),
        meta: { title: '编辑表' }
      },
      {
        path: '/lowcode/list',
        component: () => import('@/views/lowcode/list.vue'),
        meta: { title: '页面列表' }
      }
    ]
  }
]

export default route
