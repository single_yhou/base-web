/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-11 12:56:36
 * @LastEditTime: 2023-06-06 15:14:04
 * @Description:
 * @FilePath: /base-web/src/views/lowcode/api/lowcode.ts
 *
 */
import axiosReq from '@/utils/axiosReq'
import { ObjTy } from '~/common'

export interface Lowcode {
  id: number
  gmtCreate: string
  createBy: string
  code: string
  config: string
  version: number
  remark: string
  type: number
  typeName: string
}

const baseUrl = '/base/page/config'

export const select = (params: ObjTy) => {
  return axiosReq({
    url: baseUrl,
    method: 'get',
    isParams: true,
    data: params
  })
}

export const selectOne = (id: string) => {
  return axiosReq({
    url: baseUrl + `/${id}`,
    method: 'get'
  })
}

export const create = (entity: Lowcode) => {
  return axiosReq({
    url: baseUrl,
    method: 'post',
    data: entity
  })
}

export const update = (entity: Lowcode) => {
  return axiosReq({
    url: baseUrl,
    method: 'put',
    data: entity
  })
}

export const remove = (ids: Array<string>) => {
  return axiosReq({
    url: baseUrl,
    method: 'delete',
    data: ids
  })
}
