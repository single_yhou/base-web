/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-22 16:33:16
 * @LastEditTime: 2023-06-08 14:24:03
 * @Description:
 * @FilePath: /base-web/src/main.ts
 *
 */
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import '@/styles/index.scss' // global css
//import element-plus
import ElementPlus from 'element-plus'
import axios from 'axios'
import 'element-plus/dist/index.css'
import i18n from './lang'

//import VForm3
import VForm3 from '@/../lib/vform/designer.umd.js'
import '../lib/vform/designer.style.css'

import en from 'element-plus/es/locale/lang/en'
import cn from 'element-plus/es/locale/lang/zh-cn'
import tw from 'element-plus/es/locale/lang/zh-tw'

// 打印
import PrintNB from 'vue3-print-nb'

const app = createApp(App)
app.use(i18n)
// app.use(VForm3) //全局注册VForm3(同时注册了v-form-designe、v-form-render等组件)
app.use(PrintNB)
app.use(VForm3)
app.config.globalProperties.$axios = axios
/* 注意：如果你的项目中有使用axios，请用下面一行代码将全局axios复位为你的axios！！ */
window.axios = axios

// app.use(DevelopCompents, { path: 'http://localhost:5000', request: axios })
// app.use(DevelopCompents, { path: 'http://web.cloud.logwire.cn', request: axios })
// app.component('draggable', Draggable)

const lang = localStorage.getItem('language') || 'zhCN'
if (lang == 'en') {
  app.use(ElementPlus, { size: 'small', locale: en })
} else if (lang == 'zhCN') {
  app.use(ElementPlus, { size: 'small', locale: cn })
} else if (lang == 'zhTW') {
  app.use(ElementPlus, { size: 'small', locale: tw })
} else {
  // 找不到语言包 就来个默认中文
  app.use(ElementPlus, { size: 'small', locale: cn })
}

//svg-icon
//import svg-icon doc in  https://github.com/anncwb/vite-plugin-svg-icons/blob/main/README.zh_CN.md
import 'virtual:svg-icons-register'
import svgIcon from '@/icons/SvgIcon.vue'
app.component('SvgIcon', svgIcon)

//global mount moment-mini
// import $momentMini from 'moment-mini'
// app.config.globalProperties.$momentMini = $momentMini
//import global directive（unplugin-vue-components auto scan）
import directive from '@/directives'
directive(app)
//import router intercept
import './permission'

//import theme
import './theme/index.scss'
//import unocss
import 'uno.css'

//element svg icon(unplugin-vue-components auto scan)
// import ElSvgIcon from '@/components/ElSvgIcon.vue'
// app.component('ElSvgIcon', ElSvgIcon)

//error log  collection
import errorLog from '@/hooks/useErrorLog'
errorLog()

//pinia
import { createPinia } from 'pinia'
import Vue3BaiduMapGL from 'vue3-baidu-map-gl'

app.use(createPinia()).use(Vue3BaiduMapGL, {
  plugins: ['Mapvgl', 'Mapv', 'MapvThree']
})
new Promise(async (res, rej) => {
  const modules = import.meta.glob('./views/**/route.ts')
  for (const path in modules) {
    const result: any = await modules[path]()
    result.default.forEach((element) => {
      router.addRoute('', element)
    })
  }
  res(modules)
}).then((modules) => {
  app.use(router).mount('#app')
})
