  
export default {
  common: {
    search: {
      keywords: '關鍵字', // 关键字
      placeholder: {
        input: '請輸入', // 请输入
        select: '請選擇', // 请选择
        beginDate: '開始日期', // 开始日期
        endDate: '結束日期' // 结束日期
      },
      dateRange: '日期範圍', // 日期范围
      search: '搜索', // 搜索
      add: '新增', // 新增
      batchDelete: '批量刪除', // 批量删除
      print: '列印' // 打印
    },
    table: {
      serialNumber: '序號', // 序号
      operation: {
        operation: '操作', // 操作
        edit: '編輯', // 编辑
        delete: '刪除' // 删除
      }
    },
    button: {
      save: '保存', // 保存
      back: '返回', // 返回
      cancel: '取消' // 取消
    },
    message: {
      saveSuccessfully: '保存成功!', // 保存成功
      saveFailed: '保存失敗!', // 保存失败
      pleaseInput: '請輸入...', // 请输入
      sure: '確定刪除?', // 确定删除?
      deleteSuccessfully: '刪除成功!', // 删除成功!
      exitSuccessfully: '退出登錄成功!', // 退出登录成功
      editSuccessfully: '編輯成功!', // 编辑成功
      formatError: '格式不正確', //  格式不正确
      changeTo: '切換為' // 切换为
    }
  },
}
