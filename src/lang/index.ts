/*
 * @Author: Chuckie
 * @Email: chuckie@slanwell.com
 * @Date: 2023-04-13 10:53:41
 * @LastEditTime: 2023-04-14 10:39:59
 * @Description:
 * @FilePath: /web/src/lang/index.ts
 *
 */

import { createI18n } from 'vue-i18n'
import en from './en'
import zhCN from './zh-CN'
import zhTW from './zh-TW'
import settings from '../settings'

const messages = { en, zhCN, zhTW }


const i18n = createI18n({
  globalInjection: true, //如果设置true, $t() 函数将注册到全局
  legacy: false, //如果想在composition api中使用需要设置为false
  locale: settings.defaultLanguage,
  messages // set locale messages
})

const modulesen  =  import.meta.glob('../views/**/en.lang.ts') 
for(let path in modulesen){
    modulesen[path]().then((lang:any)=>{
      i18n.global.mergeLocaleMessage('en',lang.default)
    })
 
}
const moduleszhcn  =  import.meta.glob('../views/**/zh-CN.lang.ts') 
for(let path in moduleszhcn){
  moduleszhcn[path]().then((lang:any)=>{
      i18n.global.mergeLocaleMessage('zhCN',lang.default)
    })
 
}
const moduleszhtw  =  import.meta.glob('../views/**/zh-TW.lang.ts') 
for(let path in moduleszhtw){
  moduleszhtw[path]().then((lang:any)=>{
      i18n.global.mergeLocaleMessage('zhTW',lang.default)
    })
 
}
 
export default i18n
