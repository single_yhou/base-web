 
export default {
  locale: 'en',
  nav: {
    setting: 'Setting',
    dicttype: 'Dict Type',
    dataOverview: 'dataOverView'
  },

  common: {
    search: {
      keywords: 'Keywords', // 关键字
      placeholder: {
        input: 'Please input', // 请输入
        select: 'Please select', // 请选择
        beginDate: 'Begin Date', // 开始日期
        endDate: 'End Date' // 结束日期
      },
      dateRange: 'DateRange', // 日期范围
      search: 'Search', // 搜索
      add: 'Add', // 新增
      batchDelete: 'BatchDelete', // 批量删除
      print: 'Print' // 打印
    },
    table: {
      serialNumber: 'NO.', // 序号
      operation: {
        operation: 'Operation', // 操作
        edit: 'Edit', // 编辑
        delete: 'Delete' // 删除
      }
    },
    button: {
      save: 'Save', // 保存
      back: 'Back', // 返回
      cancel: 'Cancel' // 取消
    },
    message: {
      saveSuccessfully: 'Save successfully!', // 保存成功
      saveFailed: 'Save failed!', // 保存失败
      pleaseInput: 'Please input...', // 请输入
      sure: 'Are you sure to delete it ?', // 确定删除?
      deleteSuccessfully: 'Delete successfully!', // 删除成功!
      editSuccessfully: 'Edit successfully!', // 编辑成功
      exitSuccessfully: 'Exit successfully!', // 退出登录成功
      formatError: 'Format Error', //  格式不正确
      changeTo: 'ChangeTo' // 切换为
    }
  },

}
