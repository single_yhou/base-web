 
 
export default {
  nav: {
    setting: 'Setting',
    dicttyp: 'Dict Type'
  },
 
  common: {
    search: {
      keywords: '关键字', // 关键字
      placeholder: {
        input: '请输入', // 请输入
        select: '请选择', // 请选择
        beginDate: '开始日期', // 开始日期
        endDate: '结束日期' // 结束日期
      },
      dateRange: '日期范围', // 日期范围
      search: '搜索', // 搜索
      add: '新增', // 新增
      batchDelete: '批量删除', // 批量删除
      print: '打印', // 打印
      import: '导入', // 导入
      export: '导出', // 导出
    },
    table: {
      serialNumber: '序号', // 序号
      operation: {
        operation: '操作', // 操作
        edit: '编辑', // 编辑
        delete: '删除' // 删除
      }
    },
    button: {
      save: '保存', // 保存
      back: '返回', // 返回
      cancel: '取消', // 取消
      delete: '删除', // 取消
    },
    message: {
      saveSuccessfully: '保存成功!', // 保存成功
      saveFailed: '保存失败!', // 保存失败
      pleaseInput: '请输入...', // 请输入
      sure: '确定删除?', // 确定删除?
      deleteSuccessfully: '删除成功!', // 删除成功!
      editSuccessfully: '编辑成功!', // 编辑成功
      exitSuccessfully: '退出登录成功!', // 退出登录成功
      formatError: '格式不正确', //  格式不正确
      changeTo: '切换为' // 切换为
    }
  }, 
}
