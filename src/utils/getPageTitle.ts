/*
 * @Author: Chuckie
 * @Email: chuckie@slanwell.com
 * @Date: 2023-06-05 11:06:52
 * @LastEditTime: 2023-06-05 17:09:49
 * @Description:
 * @FilePath: /base-web/src/utils/getPageTitle.ts
 *
 */
import defaultSettings from '@/settings'

const title = defaultSettings.title || sessionStorage.getItem('systemName')

export default function getPageTitle(pageTitle: string) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
