import lodash from 'lodash'

/*
 * @Author: Chuckie
 * @Email: chuckie@slanwell.com
 * @Date: 2023-03-29 15:50:26
 * @LastEditTime: 2023-06-07 10:00:44
 * @Description:
 * @FilePath: /base-web/src/utils/tools.ts
 *
 */
export const print = () => {
  return {
    id: 'print-box',
    popTitle: '配置页眉标题',
    previewTitle: '预览标题',
    previewPrintBtnlabel: '预览结束，开始打印',
    beforeOpenCallback(vue: any) {
      console.log('打开前')
    },
    openCallback(vue: any) {
      console.log('执行了打印')
    },
    closeCallback(vue: any) {
      console.log('关闭了打印工具')
    }
  }
}

export const hasPermission = (permission: string): boolean => {
  const permiss = JSON.parse(sessionStorage.getItem('permission') || '')
  return lodash.includes(permiss, permission)
}
