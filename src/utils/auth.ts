/*
 * @Author: Chuckie
 * @Email: chuckie@aliyun.com
 * @Date: 2022-11-10 13:13:11
 * @LastEditTime: 2022-11-14 11:20:24
 * @Description:
 * @FilePath: /zl-web/src/utils/auth.ts
 *
 */
const TokenKey = 'Authorization'

export function getToken() {
  return localStorage.getItem(TokenKey)
}

export function setToken(token: string) {
  return localStorage.setItem(TokenKey, `Bearer ${token}`)
}

export function removeToken() {
  return localStorage.removeItem(TokenKey)
}
